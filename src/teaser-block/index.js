// source for this was: https://jasonbahl.com/2018/11/15/two-ways-to-build-gutenberg-blocks/

const { __ } = wp.i18n;
const { Component } = wp.element;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.editor;


class ByldaTeaserBlock extends Component {

    render() {

        const {
            attributes: {
                textContent
            },
            setAttributes
        } = this.props;

        return[
            <div className="container teaser" style={{
                backgroundColor: '#b93636',
				color: 'white',
				padding: '30px'
            }}>
                <RichText
                    tagName="div"
                    multiline="p"
                    placeholder={ __( 'Teaser für diese Seite' ) }
                    keepPlaceholderOnFocus
                    value={ textContent }
                    formattingControls={ [ 'bold', 'italic' ] }

                    style={ {
						color: 'white'
						
                    } }
                    onChange={ ( value ) => setAttributes( { textContent: value } ) }
                />
            </div>
        ];
    }
}

registerBlockType( 'gsc/bylda-teaser', {
    title: __( 'Teaser' ),
    icon: 'visibility',
    category: 'layout',
    keywords: [
        __( 'teaser' ),
    ],
    attributes: {
        textContent: {
            type: 'string',
            default: 'Ein Teaser-Text für diese Seite'
        },
    },
    edit: ByldaTeaserBlock,
    save: function( props ) {
        const { attributes: { textContent }} = props;
        return (
            <div id="cgb-testimonial" className="container teaser" >
                { textContent && !! textContent.length && (
                    <RichText.Content
                        tagName="p"                        
                        value={ textContent }
                    />
                )}
            </div>
        );
    },
} );
