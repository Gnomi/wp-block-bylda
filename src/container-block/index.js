const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.editor;

registerBlockType( 'gsc/bylda-block', {
    title: 'Bylda Container', // Block title. __() function allows for internationalization.
    description: 'Container to wrap content that otherwise would take full width', // Block description.
    icon: 'editor-contract', // Block icon from Dashicons. https://developer.wordpress.org/resource/dashicons/.
    category: 'layout', // Block category. Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
        
    edit( { className } ) {
        return (
            <div className={ className + ' container not-mobile'} 
                 style={{ border: '1px solid grey'}}>
                <InnerBlocks />
            </div>
        );
    },

    save() {
        return (
            <div class="container not-mobile">
                <InnerBlocks.Content />
            </div>
        );
    },
} );
