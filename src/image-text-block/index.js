( function( blocks, editor, i18n, element, components, _ ) {
	var el 					= element.createElement;
	var InspectorControls 	= editor.InspectorControls;
	var MediaUpload 		= editor.MediaUpload;
	var Fragment 			= element.Fragment;
    var registerBlockType 	= blocks.registerBlockType; // The registerBlockType() function to register blocks.
	var SelectControl		= components.SelectControl;
	var PanelBody 			= components.PanelBody;
	var InnerBlocks			= editor.InnerBlocks;

	registerBlockType( 'gsc/text-and-image', {
		title: 'Bylda text & gradient image', // Block title. __() function allows for internationalization.
        description: 'Text next to an image that is partly covered by an gradient', // Block description.
        icon: 'id', // Block icon from Dashicons. https://developer.wordpress.org/resource/dashicons/.
        category: 'layout', // Block category. Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
        attributes: {
			mediaID: {
				type: 'number',
			},
			mediaURL: {
				type: 'string',
				source: 'attribute',
				selector: 'img',
				attribute: 'src',
			},
			floatToggle: {
				type: 'string',
				default: 'left',
			},
			showImage: {
				type: 'string',
				default: 'show',
			},
			addSpacer: {
				type: 'string',
				default: 'spacer',
			},

		},
		edit: function( props ) {
			var attributes = props.attributes;
			var floatToggle = attributes.floatToggle;
			var addSpacer = attributes.addSpacer;

			var onSelectImage = function( media ) {
				return props.setAttributes( {
					mediaURL: media.url,
					mediaID: media.id,
				} );
			};

			function onChangeFloatToggle(newFloatToggle) {
				props.setAttributes({ floatToggle: newFloatToggle });
			}

			function onChangeAddSpacer(newAddSpacer) {
				props.setAttributes({ addSpacer: newAddSpacer });
			}

			return el(
				Fragment,
				null,
				el( InspectorControls, null,
					el(PanelBody, { title: "Einstellungen"},
						el( SelectControl,
							{
								label: 'Ausrichtung wählen',
								value: floatToggle,
								onChange: onChangeFloatToggle,
								options: [
									{ label: "links", value: "left"},
									{ label: "rechts", value: "right"},
									{ label: "mittig", value: "center"}
								]
							}
						),
						el( SelectControl,
							{
								label: 'Abstand oben und unten',
								value: addSpacer,
								onChange: onChangeAddSpacer,
								options: [
									{ label: "hinzufügen", value: "space"},
									{ label: "entfernen", value: ""}
								]
							}
						),
					),
				),

				el( 'div', { className: props.className },

					el( MediaUpload, {
						onSelect: onSelectImage,
						allowedTypes: 'image',
						value: attributes.mediaID,
						render: function( obj ) {
							return el( components.Button, {
									className: attributes.mediaID ? 'image-button' : 'button button-large',
									onClick: obj.open
								},
								! attributes.mediaID ? 'Bild auswählen' : el( 'img', { src: attributes.mediaURL } )
							);
						}
					} ),

					el (InnerBlocks, null)
				)
			);
		},
		save: function( props ) {
			var attributes = props.attributes;
			var floatToggle = attributes.floatToggle;
			var addSpacer = attributes.addSpacer;
			var image =  attributes.mediaURL;

			return (
				el( 'section', {className: props.className},

					attributes.mediaURL &&
						el( 'div', 
							{ 
								className: 'section-background ' + floatToggle,
								style: {backgroundImage: 'url(' + image + ')'}
							},
							el('img', { src: image })
						),
					el( 'div', {className: 'gradient-' + floatToggle }),
					el( 'div', {className: 'container gradient-foreground'}, 
						el( 'div', { className: 'float-' + floatToggle + ' ' + addSpacer },
							el(InnerBlocks.Content, null)
						)
					)
				)
			);
		},
	} );

} )(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._,
);
